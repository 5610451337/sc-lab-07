
public class Controls {
	private TheaterManagement theater;
	
	
	public Controls(){
		TheaterManagement theaterData = new TheaterManagement(DataSeatPrice.ticketPrices);
		theater = theaterData;
		Testcase();
	}
	
	
	public static void main(String[] args){
		new Controls();
	}
	
	
	public void Testcase(){
		System.out.println("Test A");
		System.out.println("Buy O 11 and 12");
		System.out.println(theater.getTotal("O 11 O 12"));
		
		
		System.out.println("\n"+"Test B");
		System.out.println("Buy ticket prices 20 and 50");
		System.out.println(theater.getTotalbyPrices("20 50"));
		
		System.out.println("\n"+"Test C");
		System.out.println("Check O 12 and O 13");
		System.out.println(theater.getCheckseat("O 12 O 13"));
		
		
		
		
	}

}
