
public class TheaterManagement {
	private double ticketPrices[][];	
	public TheaterManagement(double[][] ticketPrices){
		this.ticketPrices = ticketPrices;
	}
	public String getCheckseat(String seat){
		String check="";
		String[] x = seat.split(" ");
		for(int i = 0;i<x.length;i=i+2){
			char rolls =x[i].charAt(0);
			int nRolls =rolls -65;
			int seats = Integer.parseInt(x[i+1]);
			for(int k =0;k<ticketPrices.length;k++){
				if(nRolls == k){
					for(int l =0;l<ticketPrices[k].length;l++){
						if(ticketPrices[k][l]>1 && seats ==l){
							check = check +"True ";
						}
						else if(seats==l){
							check = check+"Flase ";
						}
					}
					
				}
			}
		}
		return check;	
	}
	
	public double getTotal(String buyseat ){
		double prices=0;
		String[] x =buyseat.split(" ");
		for(int i=0;i<x.length;i=i+2){
			char rolls= x[i].charAt(0);
			int nRolls = rolls -65;
			int seats = Integer.parseInt(x[i+1]);
			for(int k = 0;k< ticketPrices.length;k++){
				if(nRolls==k){
					for(int l =0;l<ticketPrices[k].length;l++){
						if(l==seats-1){
							prices = prices+ticketPrices[k][l];
							ticketPrices[k][l]=0;
						}
					}
				}
				
			}
		}
		return prices;
		
	}
	public String getTotalbyPrices(String buyprices){
		double prices=0;
		String seat = "";
		String[] x =buyprices.split(" ");
		for(int i = 0;i<x.length;i++){
			double pris  = Double.parseDouble(x[i]);
//			String seat = "";
			for(int k = 0;k<ticketPrices.length;k++){
				for(int l =0;l<ticketPrices[k].length;l++){
					if( pris == ticketPrices[k][l] ){
						char rolls = (char)(k+65);
						int seats = l+1;
						String snr =rolls + Integer.toString(seats);
						seat = seat+snr+" ";
						prices = prices+ticketPrices[k][l];
						ticketPrices[k][l]=0;
					}
					else{
						seat=seat+"   ";
					}
				}
				seat = seat+"\n";
			}
			
		}
		return seat;
	}
}